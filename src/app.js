console.log('App.js is running!');

// JSX - JavaScript XML 

var book = {
    title: 'Lord Of The Rings',
    subtitle: 'The Fellowship of the Ring'
};
var template = (
    <div>
        <h1>{book.title}</h1>
        <p>{book.subtitle}</p>
        <ol>
            <li>Item one</li>
            <li>Item two</li>    
        </ol>
    </div>
);

var user = {
    name: 'Andrew',
    age: 26,
    location: 'Philadelphia'
};

var templateTwo = (
    <div>
        <h1>{user.name + '!'}</h1>
        <p>Age: {user.age}</p>
        <p>Location: {user.location}</p>
       
    </div>
);
var appRoot = document.getElementById('app')

ReactDOM.render(template, appRoot);