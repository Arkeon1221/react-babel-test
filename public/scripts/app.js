'use strict';

console.log('App.js is running!');

// JSX - JavaScript XML 

var book = {
    title: 'Lord Of The Rings',
    subtitle: 'The Fellowship of the Ring'
};
var template = React.createElement(
    'div',
    null,
    React.createElement(
        'h1',
        null,
        book.title
    ),
    React.createElement(
        'p',
        null,
        book.subtitle
    ),
    React.createElement(
        'ol',
        null,
        React.createElement(
            'li',
            null,
            'Item one'
        ),
        React.createElement(
            'li',
            null,
            'Item two'
        )
    )
);

var user = {
    name: 'Andrew',
    age: 26,
    location: 'Philadelphia'
};

var templateTwo = React.createElement(
    'div',
    null,
    React.createElement(
        'h1',
        null,
        user.name + '!'
    ),
    React.createElement(
        'p',
        null,
        'Age: ',
        user.age
    ),
    React.createElement(
        'p',
        null,
        'Location: ',
        user.location
    )
);
var appRoot = document.getElementById('app');

ReactDOM.render(template, appRoot);
